#include "reliefdialdialog.h"
#include <QLayout>
#include <QDial>
#include <QDebug>

ReliefDialDialog::ReliefDialDialog(QWidget* parent):QDialog(parent)
{
    this->setMinimumSize(200, 200);
    QHBoxLayout* bwLayout = new QHBoxLayout();
    QDial* angSlider = new QDial(this);
//    angSlider->setRange(0, 255);
    bwLayout->addWidget(angSlider);
    this->setLayout(bwLayout);
    connect(angSlider, SIGNAL(valueChanged(int)), this, SLOT(onSliderValueChanged(int)));
//    bwSlider->setValue(128);
//    this->setWindowModality(Qt::WindowModal);
}

void ReliefDialDialog::onSliderValueChanged(int value) {
    emit this->sliderValueChanged(float(value) * 3.6);
}

