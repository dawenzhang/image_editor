#ifndef RELIEFDIALDIALOG_H
#define RELIEFDIALDIALOG_H

#include <QDialog>

class ReliefDialDialog: public QDialog
{
    Q_OBJECT
public:
    explicit ReliefDialDialog(QWidget* parent = 0);

signals:
    void sliderValueChanged(float);

public slots:
    void onSliderValueChanged(int);
};


#endif // RELIEFDIALDIALOG_H
