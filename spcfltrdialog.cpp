#include "spcfltrdialog.h"
#include <QLayout>
#include <QTextEdit>
#include <QDialogButtonBox>
#include <QDebug>

SpcFltrDialog::SpcFltrDialog(QWidget* parent) : QDialog(parent)
{
    this->setLayout(new QVBoxLayout);
    this->filter = new QTextEdit(this);
    this->buttonBox = new QDialogButtonBox(this);
    this->buttonBox->setStandardButtons(QDialogButtonBox::Ok|QDialogButtonBox::Cancel);
    this->layout()->addWidget(this->filter);
    this->layout()->addWidget(this->buttonBox);
    this->connect(this->buttonBox, SIGNAL(accepted()), this, SLOT(buttonBoxAccepted()));
    this->connect(this->buttonBox, SIGNAL(rejected()), this, SLOT(close()));
}

void SpcFltrDialog::buttonBoxAccepted() {
    QVector<double> data;
    QString strMat = this->filter->toPlainText().trimmed();
    strMat.replace("/n","");
    int nCols = -1;
    bool ok;
    QStringList rows = strMat.split(QRegExp("\\s*;"));
    for(int i = 0; i < rows.size(); i ++) {
        if (rows[i].trimmed().isEmpty()) {
            continue;
        }
        if(nCols < 0) {
            readOneRow(data,rows[i].trimmed(),nCols,ok);
            if(!ok) {
                qDebug()<<"parse error!";
                return;
            }
        }else {
            int tmp;
            readOneRow(data, rows[i].trimmed(), tmp, ok);
            if(!ok) {
                qDebug()<<"parse error!";
                return;
            }
            if(tmp != nCols) {
                qDebug()<<"number of culums are not equals";
                return;
            }
        }
    }
    emit okClicked(data, nCols);
    this->close();
}

void SpcFltrDialog::readOneRow(QVector<double>& data, QString row, int& nCols, bool &ok) {
    nCols = 0;
    QStringList cols = row.split(QRegExp("\\s+|\\s*,"));
    for(int i = 0; i < cols.size(); i ++) {
        if(cols[i].trimmed().isEmpty()) {
            continue;
        }
        double d = cols[i].toDouble(&ok);
        if(!ok) {
            return;
        }
        data.push_back(d);
        nCols ++;
    }
}
