#ifndef MYPIXMAPITEM_H
#define MYPIXMAPITEM_H

#include <QGraphicsPixmapItem>

class MyPixmapItem : public QGraphicsPixmapItem
{

public:
    explicit MyPixmapItem(QGraphicsItem *parent = 0);
    explicit MyPixmapItem(const QPixmap &pixmap, QGraphicsItem *parent = 0);

    void reset();

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent* event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent* event);
    void wheelEvent(QGraphicsSceneWheelEvent* event);

signals:

public slots:

};

#endif // MYPIXMAPITEM_H
