#include "imageprocessor.h"
#include <QImage>
#include <QtMath>
#include <QMap>
#include <QPoint>
#include <QColor>

#include "tools.h"

#include <QDebug>

ImageProcessor::ImageProcessor(QImage img, QObject* parent) : QObject(parent)
{
    this->save = false;
    this->_img = img;
}

void ImageProcessor::setImage(QImage image) {
    this->_img = image;
}

QImage ImageProcessor::rgb2gray() {
    QImage grayImage;
    grayImage = this->_img;
    int width = this->_img.width();
    int height = this->_img.height();
    for(int i = 0; i < width; i ++) {
        for(int j = 0; j < height; j ++) {
            QRgb rgb = this->_img.pixel(i, j);
            int r = qRed(rgb);
            int g = qGreen(rgb);
            int b = qBlue(rgb);
            QRgb gray = qRgb((r + g + b) / 3, (r + g + b) / 3, (r + g + b) / 3);
            grayImage.setPixel(i, j, gray);
        }
    }
    this->tempSave(grayImage);
    return grayImage;
}

QImage ImageProcessor::rgb2bw(int value) {
    QImage bwImage;
    bwImage = this->_img;
    int width = this->_img.width();
    int height = this->_img.height();
    for(int i = 0; i < width; i ++) {
        for(int j = 0; j < height; j ++) {
            QRgb rgb = this->_img.pixel(i, j);
            int r = qRed(rgb);
            int g = qGreen(rgb);
            int b = qBlue(rgb);
            int gray = (r + g + b) / 3;
            if(gray > value) {
                QRgb bw = qRgb(255, 255, 255);
                bwImage.setPixel(i, j, bw);
            }else {
                QRgb bw = qRgb(0, 0, 0);
                bwImage.setPixel(i, j, bw);
            }
        }
    }
    this->tempSave(bwImage);
    return bwImage;
}

QImage ImageProcessor::negative() {
    QImage negativeImage;
    negativeImage = this->_img;
    int width = this->_img.width();
    int height = this->_img.height();
    for(int i = 0; i < width; i ++) {
        for(int j = 0; j < height; j ++) {
            QRgb rgb = this->_img.pixel(i, j);
            int r = qRed(rgb);
            int g = qGreen(rgb);
            int b = qBlue(rgb);
            QRgb negatived = qRgb(255 - r, 255 - g, 255 - b);
            negativeImage.setPixel(i, j, negatived);
        }
    }
    this->tempSave(negativeImage);
    return negativeImage;
}

QImage ImageProcessor::stretch(int value1, double value2) {
    QImage strechImage;
    strechImage = this->_img;
    int width = this->_img.width();
    int height= this->_img.height();
    for(int i = 0; i < width; i ++) {
        for(int j = 0; j < height; j ++) {
            QRgb rgb = this->_img.pixel(i, j);
            int r = qRed(rgb);
            int g = qGreen(rgb);
            int b = qBlue(rgb);
            int gray = 0.299 * r + 0.587 * g + 0.114 * b;
            int streched = 255 / (1 + qPow(value1 / (0.0000001 + gray), value2));
            strechImage.setPixel(i, j, qRgb(streched, streched, streched));
        }
    }
    this->tempSave(strechImage);
    return strechImage;
}

QImage ImageProcessor::log(int n) {
    QImage logImage;
    logImage = this->_img;
    int width = this->_img.width();
    int height= this->_img.height();
    for(int i = 0; i < width; i ++) {
        for(int j = 0; j < height; j ++) {
            QRgb rgb = this->_img.pixel(i, j);
            int r = qRed(rgb);
            int g = qGreen(rgb);
            int b = qBlue(rgb);
            int gray = 0.299 * r + 0.587 * g + 0.114 * b;
            int log = n * qLn( 1 + gray);
            logImage.setPixel(i, j, qRgb(log, log, log));
        }
    }
    this->tempSave(logImage);
    return logImage;
}

QImage ImageProcessor::histogramEqualize() {
    QImage histogramEqualizedImage;
    histogramEqualizedImage = this->_img;
    int width = this->_img.width();
    int height= this->_img.height();
    int pixelsAmount = width * height;
    int sum = 0;
    int histogram[256] = {0};
    QMap<int, int> histogramMap;
    for(int i = 0; i < width; i ++) {
        for(int j = 0; j < height; j ++) {
            QRgb rgb = this->_img.pixel(i, j);
            int r = qRed(rgb);
            int g = qGreen(rgb);
            int b = qBlue(rgb);
            int gray = (r + g + b) / 3;
            histogram[gray] ++;
        }
    }

    for(int i = 0; i < 256; i ++) {
        sum += histogram[i];
        histogramMap.insert(i, qRound(255 * ((double)sum / pixelsAmount)));
    }

    for(int i = 0; i < width; i ++) {
        for(int j = 0; j < height; j ++) {
            QRgb rgb = this->_img.pixel(i, j);
            int r = qRed(rgb);
            int g = qGreen(rgb);
            int b = qBlue(rgb);
            int gray = (r + g + b) / 3;
            int histogramEqualizedValue = histogramMap.value(gray);
            histogramEqualizedImage.setPixel(i, j, qRgb(histogramEqualizedValue, histogramEqualizedValue, histogramEqualizedValue));
        }
    }
    this->tempSave(histogramEqualizedImage);
    return histogramEqualizedImage;
}

QImage ImageProcessor::histogramExactSpecifiedEqualize() {
    QImage histogramExactSpecifiedEqualizedImage;
    histogramExactSpecifiedEqualizedImage = this->_img;
    int width = this->_img.width();
    int height= this->_img.height();
    int pixelsAmount = width * height;
    int average = pixelsAmount / 256;
    QMap<int, QPoint> histogramMap;
    for(int i = 0; i < width; i ++) {
        for(int j = 0; j < height; j ++) {
            QRgb rgb = this->_img.pixel(i, j);
            int r = qRed(rgb);
            int g = qGreen(rgb);
            int b = qBlue(rgb);
            int gray = (r + g + b) / 3;
            histogramMap.insertMulti(gray, QPoint(i, j));
        }
    }

    QList<QPoint> sortedList;

    for(int i = 0; i < 256; i ++) {
//        qDebug()<<histogramMap.values(i).length();
        Tools::sortAndAdd2List(this->_img, histogramMap.values(i), &sortedList);
//        Tools::sortAndAdd2List(_img, histogramMap.values(i), &sortedList);
    }

//    qDebug()<<pixelsAmount;
//    qDebug()<<sortedList.length();
//    return histogramExactSpecifiedEqualizedImage;

    for(int i = 0; i < width; i ++) {
        for(int j = 0; j < height; j ++) {
            histogramExactSpecifiedEqualizedImage.setPixel(i, j, qRgb(255, 255, 255));
        }
    }

    for(int i = 0; i < 256; i ++) {
        for(int j = 0; j < average; j ++) {
            histogramExactSpecifiedEqualizedImage.setPixel(sortedList.at(i * average + j), qRgb(i, i, i));
        }
    }
    this->tempSave(histogramExactSpecifiedEqualizedImage);
    return histogramExactSpecifiedEqualizedImage;
}

QImage ImageProcessor::spatialFilter(QVector<double> data, int nCols) {
    int nRows = data.size() / nCols;
    if(nRows % 2 != 1 || nCols % 2 != 1) {
     qDebug()<<"nCols and nRows should be odd";
     return this->_img;
    }
    this->tempSave(crop(filter(zeroPadding(this->_img, nCols, nRows), data, nCols), nCols, nRows));
    return crop(filter(zeroPadding(this->_img, nCols, nRows), data, nCols), nCols, nRows);
}

QImage ImageProcessor::zeroPadding(QImage origin, int nCols, int nRows) {
    QImage image = QImage(origin.width() + nCols - 1, origin.height() + nRows - 1, origin.format());
    int width = image.width();
    int height = image.height();
    for(int i = 0; i < width; i ++) {
        for(int j = 0; j < height; j ++)
        {
            if(i < nCols / 2 || i >= nCols / 2 + origin.width() || j < nRows / 2 || j >= nRows / 2 + origin.height()) {
                image.setPixel(i, j, qRgb(0, 0, 0));
            }else {
             QRgb rgb = origin.pixel(i - nCols / 2, j - nRows / 2);
             int gray = (qRed(rgb) + qGreen(rgb) + qBlue(rgb)) / 3;
             image.setPixel(i, j, qRgb(gray, gray, gray));
            }
        }
    }
    return image;
//    img=image;
}

QImage ImageProcessor::replicatePadding(QImage origin, int nCols, int nRows) {

}

QImage ImageProcessor::symmetricPadding(QImage origin, int nCols, int nRows) {
    QImage image = QImage(origin.width() + nCols - 1, origin.height() + nRows - 1,origin.format());
    int width = image.width();
    int height = image.height();
    for(int i = 0; i < width; i ++) {
        for(int j = 0; j < height; j ++) {
            if(i < nCols / 2 || i >= nCols / 2 + origin.width() || j < nRows / 2 || j >= nRows / 2 + origin.height()) {
                    if(i < nCols / 2 && j < nRows / 2) {
                    QRgb rgb = origin.pixel(1 - i, 1 - j);
                    int gray = (qRed(rgb) + qGreen(rgb) + qBlue(rgb)) / 3;
                    image.setPixel(i, j, qRgb(gray, gray, gray));
                    } else if(i < nCols / 2 && j < nRows / 2 + origin.height()) {
                    QRgb rgb = origin.pixel(1 - i, j - nRows / 2);
                    int gray = (qRed(rgb) + qGreen(rgb) + qBlue(rgb)) / 3;
                    image.setPixel(i, j, qRgb(gray, gray, gray));
                    } else if(i < nCols / 2) {
                    QRgb rgb = origin.pixel(1 - i,2 * origin.height() + nRows / 2 - j - 1);
                    int gray = (qRed(rgb) + qGreen(rgb) + qBlue(rgb)) / 3;
                    image.setPixel(i, j, qRgb(gray, gray, gray));
                    }else if(j >= nRows / 2 + origin.height() && i < nCols / 2 + origin.width()) {
                    QRgb rgb = origin.pixel(i - nCols / 2, 2 * origin.height() + nRows / 2 - j - 1);
                    int gray = (qRed(rgb) + qGreen(rgb) + qBlue(rgb)) / 3;
                    image.setPixel(i, j, qRgb(gray, gray, gray));
                    }else if(j >= nRows / 2 + origin.height()) {
                    QRgb rgb = origin.pixel(2 * origin.width() + nCols / 2 - i - 1, 2 * origin.height() + nRows / 2 - j - 1);
                    int gray = (qRed(rgb) + qGreen(rgb) + qBlue(rgb)) / 3;
                    image.setPixel(i, j, qRgb(gray, gray, gray));
                    }else if(i >= nCols / 2 + origin.width() && j >= nRows / 2) {
                    QRgb rgb = origin.pixel(2 * origin.width() + nCols / 2 - i - 1, j - nRows / 2);
                    int gray = (qRed(rgb) + qGreen(rgb) + qBlue(rgb)) / 3;
                    image.setPixel(i, j, qRgb(gray, gray, gray));
                    }else if(i >= nCols / 2 + origin.width()) {
                    QRgb rgb = origin.pixel(2 * origin.width() + nCols / 2 - i - 1, 1 - j) ;
                    int gray =(qRed(rgb) + qGreen(rgb) + qBlue(rgb)) / 3;
                    image.setPixel(i, j, qRgb(gray, gray, gray));
                    }else {
                    QRgb rgb = origin.pixel(i - nCols / 2, 1 - j) ;
                    int gray = (qRed(rgb) + qGreen(rgb) + qBlue(rgb)) / 3;
                    image.setPixel(i, j, qRgb(gray, gray, gray));
                    }
            }else {
             QRgb rgb = origin.pixel(i - nCols / 2, j - nRows / 2);
             int gray =(qRed(rgb) + qGreen(rgb) + qBlue(rgb)) / 3;
             image.setPixel(i, j, qRgb(gray, gray, gray));
            }
        }
    }
    return image;
//    img=image;
}

QImage ImageProcessor::filter(QImage origin, QVector<double> data, int nCols) {
    QImage image = origin;
    int nRows = data.size() / nCols;
    int width = origin.width();
    int height = origin.height();
    for(int i = nCols / 2; i < width - nCols / 2; i ++) {
        for(int j = nRows / 2; j < height - nRows / 2; j ++) {
            QVector<int> patch = getPatch(origin, i, j, nRows, nCols);
            int sum = 0;
            for(int k = 0; k < data.size(); k ++) {
                sum += patch[k] * data[k] / 140 + 128;
            }
            image.setPixel(i, j, qRgb(sum / data.size(), sum / data.size(), sum / data.size()));
        }
    }
//    img=image;
    return image;
}

QVector<int> ImageProcessor::getPatch(QImage origin, int i, int j, int nRows, int nCols) {
  QVector<int> patch;
  for(int jj = j - nRows / 2; jj <= j + nRows / 2; jj ++) {
      for(int ii = i - nCols / 2; ii <= i + nCols / 2; ii ++) {
          QRgb rgb = origin.pixel(ii, jj);
          int gray = qGray(rgb);
          patch.push_back(gray);
      }
  }
  return patch;
}

QImage ImageProcessor::crop(QImage origin, int nCols, int nRows) {
  QImage image(origin.width() - nCols + 1, origin.height() - nRows + 1, origin.format());
  int width = origin.width();
  int height = origin.height();
  for(int i = nCols / 2; i < width - nCols / 2; i ++) {
      for(int j = nRows / 2; j < height - nRows / 2; j ++) {
          QRgb rgb = origin.pixel(i, j);
          int gray = (qRed(rgb) + qGreen(rgb) + qBlue(rgb)) / 3;
          image.setPixel(i - nCols / 2, j - nCols / 2, qRgb(gray, gray, gray));
      }
  }
//  this->_img=image;
  return image;
}

QImage ImageProcessor::medianFilter(QVector<double> data, int nCols) {
    int nRows = data.size() / nCols;
    QImage image = QImage(symmetricPadding(this->_img, nCols, nRows));
    int width = image.width();
    int height = image.height();
    QImage tempImage = image;
    for(int i = nCols / 2; i < width - nCols / 2; i ++) {
        for(int j = nRows / 2; j < height - nRows / 2; j ++) {
            QVector<int> patch = getPatch(tempImage, i, j, nRows, nCols);
            qSort(patch.begin(), patch.end());
            int sum = patch[patch.size( ) / 2 - 1];
            image.setPixel(i, j, qRgb(sum, sum, sum));
        }
    }
    this->tempSave(image);
//    img=image;
    return image;
}

QImage ImageProcessor::fft() {
    this->tempSave(*Tools::fftImage2Gray(&this->_img));
    return *Tools::fftImage2Gray(&this->_img);
}

QImage ImageProcessor::oilPaint(int v1, int v2) {
    QImage src = this->_img;
    QImage dst = src;
    int radius = v1;
    float fIntensityLevels = v2;
        int width = src.width();
        int height = src.height();
        for(int i = 0; i < width; i ++)
        {
            for(int j = 0; j < height; j ++)
            {
                int nIntensityCount[256];
                for(int p = 0; p < 256; p ++){
                    nIntensityCount[p] = 0;
                }
                int nSumR[256];
                for(int p = 0; p < 256; p ++){
                    nSumR[p] = 0;
                }
                int nSumG[256];
                for(int p = 0; p < 256; p ++){
                    nSumG[p] = 0;
                }
                int nSumB[256];
                for(int p = 0; p < 256; p ++){
                    nSumB[p] = 0;
                }
                for(int m = -radius; m < radius; m ++){
                    for(int n = -radius; n < radius; n ++){
                        if((i + m) < 0 || (i + m) >= width || (j + n) < 0 || (j + n) >= height){

                        }else{
                            int nR = qRed(src.pixel(i + m, j + n));
                            int nG = qGreen(src.pixel(i + m, j + n));
                            int nB = qBlue(src.pixel(i + m, j + n));
                            int nCurIntensity = (((nR + nG + nB) / 3.0) * fIntensityLevels) / 255;
                            nCurIntensity = (255 < nCurIntensity ? 255 : nCurIntensity);
                            int q = nCurIntensity;
                            nIntensityCount[q] ++;
                            nSumR[q] += nR;
                            nSumG[q] += nG;
                            nSumB[q] += nB;
                        }
                    }
                }
                int nCurMax = 0;
                int nMaxIndex = 0;
                for(int nI = 0; nI < 256; nI ++){
                    if(nIntensityCount[nI] > nCurMax){
                        nCurMax = nIntensityCount[nI];
                        nMaxIndex = nI;
                    }
                }
                dst.setPixel(i, j, qRgb(nSumR[nMaxIndex] / nCurMax, nSumG[nMaxIndex] / nCurMax, nSumB[nMaxIndex] / nCurMax));
            }
        }
        this->tempSave(dst);
        return dst;
}

QImage ImageProcessor::relief(float ang) {
    float a = ang;
    float arc = a * 3.14 / 180 + 1.57;
    QVector<double> kt;
    kt << 100 * qSin(arc - 2.35) << 140 * qSin(arc - 1.57) << 100 * qSin(arc - 0.78) << 140 * qSin(arc - 3.14) << 0 << 140 * qSin(arc - 0) << 100 * qSin(arc - 3.92) << 140 * qSin(arc - 4.71) << 100 * qSin(arc - 5.49);
    QImage src = this->rgb2gray();
    this->tempSave(this->filter(src, kt, 3));
    return this->filter(src, kt, 3);
}

QImage ImageProcessor::paddingByEdge(int nCols, int nRows) {
    QImage image = QImage(this->_img.width() + nCols - 1, this->_img.height() + nRows - 1, this->_img.format());
    int width = image.width();
    int height = image.height();
    for(int i = 0; i < width; i ++) {
        for(int j = 0; j < height; j ++) {
            QRgb rgb;
            int colf;
            int rowf;
            if(i < nCols / 2) {
                colf = 0;
            }
            if(i >= width - nCols / 2) {
                colf = this->_img.width() - 1;
            }
            if(i >= nCols / 2 && i < width - nCols / 2) {
                colf = i - nCols / 2;
            }
            if(j < nRows / 2) {
                rowf = 0;
            }
            if(j >= height - nRows / 2) {
                rowf = this->_img.height() - 1;
            }
            if(j >= nRows / 2 && j < height - nRows / 2) {
                rowf = j - nRows / 2;
            }
            rgb = this->_img.pixel(colf, rowf);
            image.setPixel(i, j, rgb);
        }
    }
    return image;
}

QImage ImageProcessor::edgeExtraction() {
    return this->cropColor(this->edgeKernel(this->paddingByEdge(3, 3)), 3, 3);
}

QImage ImageProcessor::edgeKernel(QImage image) {
    QImage image1 = QImage(image.width(), image.height(), image.format());
    int kernel1 [3][3]= {{- 1, 0, 1}, {- 2, 0, 2}, {- 1, 0, 1}};
    int kernelSize1 = 3;
    for(int x = kernelSize1 / 2; x < image.width() - kernelSize1 / 2; x ++) {
        for(int y = kernelSize1 / 2; y < image.height() - kernelSize1 / 2; y ++) {
            int r1 = 0;
            int g1 = 0;
            int b1 = 0;
            QColor color1;
            for(int i = - kernelSize1 / 2; i <= kernelSize1 / 2; i ++) {
                for(int j = - kernelSize1 / 2; j <= kernelSize1 / 2; j ++) {
                    color1 = QColor(image.pixel(x + i, y + j));
                    r1 += color1.red() * kernel1[kernelSize1 / 2 + i][kernelSize1 / 2 + j];
                    g1 += color1.green() * kernel1[kernelSize1 / 2 + i][kernelSize1 / 2 + j];
                    b1 += color1.blue() * kernel1[kernelSize1 / 2 + i][kernelSize1 / 2 + j];
                }
            }
            r1 = qBound(0, r1, 255);
            g1 = qBound(0, g1, 255);
            b1 = qBound(0, b1, 255);
            int gray1 = r1 * 0.299 + g1 * 0.587 + b1 * 0.114;
            image1.setPixel(x, y, qRgb(gray1, gray1, gray1));
        }
    }

    QImage image2 = QImage(image.width(), image.height(), image.format());
    int kernel2 [3][3] = {{- 1, - 2, - 1}, {0, 0, 0}, {1, 2, 1}};
    int kernelSize = 3;
    for(int x = kernelSize / 2; x < image.width() - kernelSize / 2; x ++) {
        for(int y = kernelSize / 2; y < image.height() - kernelSize / 2; y ++) {
           int r = 0;
           int g = 0;
           int b = 0;
           QColor color2;
           for(int i = - kernelSize / 2; i <= kernelSize / 2; i ++) {
               for(int j = -kernelSize / 2; j <= kernelSize / 2; j ++) {
                   color2 = QColor(image.pixel(x + i, y + j));
                   r += color2.red() * kernel2[kernelSize / 2 + i][kernelSize / 2 + j];
                   g += color2.green() * kernel2[kernelSize1 / 2 + i][kernelSize1 / 2 + j];
                   b += color2.blue() * kernel2[kernelSize1 / 2 + i][kernelSize1 / 2 + j];
               }
           }
           r = qBound(0, r, 255);
           g = qBound(0, g, 255);
           b = qBound(0, b, 255);
           int gray = r * 0.299 + g * 0.587 + b * 0.114;
           image2.setPixel(x, y, qRgb(gray, gray, gray));
        }
     }

    QImage image3 = QImage(image.width(), image.height(), image.format());
    for(int i = 0; i < image.width(); i ++) {
        for(int j = 0; j < image.height(); j ++) {
            QRgb rgb1 = image1.pixel(i, j);
            QRgb rgb2 = image2.pixel(i, j);

            int r1 = qRed(rgb1);
            int r2 = qRed(rgb2);
            int res = qSqrt(r1 * r1 + r2 * r2);
            res = qBound(0, res, 255);
            image3.setPixel(i, j, qRgb(res, res, res));
        }
    }
    return image3;
}

QImage ImageProcessor::cropColor(QImage image, int nCols, int nRows) {
    QImage img = QImage(image.width() - nCols + 1, image.height() - nRows + 1, image.format());
    int width = image.width();
    int height = image.height();
    for(int i = nCols / 2; i < width - nCols / 2; i ++) {
        for(int j = nRows / 2; j < height - nRows / 2; j ++) {
            QRgb rgb = image.pixel(i, j);
            int r = qRed(rgb);
            int g = qGreen(rgb);
            int b = qBlue(rgb);
            img.setPixel(i - nCols / 2, j - nRows / 2, qRgb(r, g, b));
        }
    }
    this->tempSave(img);
    return img;
}

QImage ImageProcessor::gaussianBlur(int radius) {
    double sigma = (double)radius / 3.0;
    double scale = - 0.5 / (sigma * sigma);
    double cons = -scale / M_PI;

    double** kernel = new double* [2 * radius + 1];
    for(int i = 0; i < 2 * radius + 1; i ++) {
            kernel[i] = new double[2 * radius + 1];
    }
    double sum = 0;
    for(int i = 0; i < (2 * radius + 1); i ++) {
        for(int j = 0; j < (2 * radius + 1); j ++) {
            int x = i - radius;
            int y = j - radius;
            kernel[i][j] = cons * exp(scale * (x * x + y * y));
            sum += kernel[i][j];
        }
    }

    for(int i = 0; i < (2 * radius + 1); i ++) {
        for(int j = 0; j < (2 * radius + 1); j ++) {
             kernel[i][j] = kernel[i][j] / sum;
        }
    }

    QImage image = this->_img;
    int kernelSize = 2 * radius + 1;
    for(int x = 0; x < image.width(); x ++) {
        for(int y = 0; y < image.height(); y ++) {
           double r = 0;
           double g = 0;
           double b = 0;
           QColor color;
           for(int i = - kernelSize / 2; i <= kernelSize / 2; i ++) {
               for(int j = - kernelSize / 2; j <= kernelSize / 2; j ++) {
                   if(x + i > 0 && x + i < this->_img.width() && y + j > 0 && y + j < this->_img.height()) {
                       color = QColor(this->_img.pixel(x + i, y + j));
                   }else {
                       color = QColor(this->_img.pixel(x, y));
                   }
                   r += color.red() * kernel[kernelSize / 2 + i][kernelSize / 2 + j];
                   g += color.green() * kernel[kernelSize / 2 + i][kernelSize / 2 + j];
                   b += color.blue() * kernel[kernelSize / 2 + i][kernelSize / 2 + j];
               }
           }
           image.setPixel(x, y, qRgb(r, g, b));
        }
     }
    this->tempSave(image);
    return image;
}

QImage ImageProcessor::expansionKernelSet(QVector<double> data, int nCols) {
    int nRows = data.size() / nCols;
    int width = this->_img.width();
    int height = this->_img.height();

    QImage image = QImage(this->_img.width(), this->_img.height(), this->_img.format());
    for(int i = nCols / 2; i < width - nCols / 2; i ++) {
        for(int j = nRows / 2; j < height - nRows / 2; j ++) {
            QRgb rgb = this->_img.pixel(i, j);
            int r = qRed(rgb);
            int g = qGreen(rgb);
            int b = qBlue(rgb);

            QVector<int> patch = this->getPatch(this->_img, i, j, nCols, nRows);
            int sum = 0;
            for(int k = 0; k < (data.size()); k ++) {
                sum += patch[k] * data[k];
            }
            if(sum != 0) {
                image.setPixel(i, j, qRgb(255, 255, 255));
            }else {
                image.setPixel(i, j, qRgb(r, g, b));
            }
        }
    }
    return image;
}

QImage ImageProcessor::expansionOp(QVector<double> data, int nCols) {
    this->tempSaveNow(this->paddingByEdge(nCols, data.size() / nCols));
    this->tempSaveNow(this->rgb2bw(128));
    this->tempSaveNow(this->expansionKernelSet(data, nCols));
    this->tempSaveNow(this->crop(this->_img, nCols, data.size() / nCols));
    this->tempSaveNow(this->rgb2bw(128));
    return this->_img;
}

QImage ImageProcessor::corrosionKernelSet(QVector<double> data, int nCols) {
    int nRows = data.size() / nCols;
    int width = this->_img.width();
    int height = this->_img.height();
    int a = 0;
    for(int n = 0; n < data.size(); n ++) {
        if(data[n] == 1) {
            a ++;
        }
    }

    QImage image = QImage(this->_img.width(), this->_img.height(), this->_img.format());
    for(int i = nCols / 2; i < width - nCols / 2; i ++) {
        for(int j = nRows / 2; j < height - nRows / 2; j ++) {
            QRgb rgb = this->_img.pixel(i, j);
            int r = qRed(rgb);
            int g = qGreen(rgb);
            int b = qBlue(rgb);

            QVector<int> patch = this->getPatch(this->_img, i, j, nCols, nRows);
            int sum = 0;
            for(int k = 0; k < (data.size()); k ++) {
                sum += patch[k] * data[k];
            }
            if(sum < a * 250) {
                image.setPixel(i, j, qRgb(0, 0, 0));
            }else {
                image.setPixel(i, j, qRgb(r, g, b));
            }
        }
    }
    return image;
}

QImage ImageProcessor::corrosionOp(QVector<double> data, int nCols) {
    this->tempSaveNow(this->paddingByEdge(nCols, data.size() / nCols));
    this->tempSaveNow(this->rgb2bw(128));
    this->tempSaveNow(this->corrosionKernelSet(data, nCols));
    this->tempSaveNow(this->crop(this->_img, nCols, data.size() / nCols));
    this->tempSaveNow(this->rgb2bw(128));
    return this->_img;
}


QImage ImageProcessor::openOp(QVector<double> data, int nCols) {
    this->tempSaveNow(this->paddingByEdge(nCols, data.size() / nCols));
    this->tempSaveNow(rgb2bw(128));
    this->tempSaveNow(this->corrosionKernelSet(data, nCols));
    this->tempSaveNow(this->expansionKernelSet(data, nCols));
    this->tempSaveNow(this->crop(this->_img, nCols, data.size() / nCols));
    this->tempSaveNow(rgb2bw(128));
    return this->_img;
}

QImage ImageProcessor::closeOp(QVector<double> data, int nCols) {
    this->tempSaveNow(this->paddingByEdge(nCols, data.size() / nCols));
    this->tempSaveNow(rgb2bw(128));
    this->tempSaveNow(this->expansionKernelSet(data, nCols));
    this->tempSaveNow(this->corrosionKernelSet(data, nCols));
    this->tempSaveNow(this->crop(this->_img, nCols, data.size() / nCols));
    this->tempSaveNow(rgb2bw(128));
    return this->_img;
}

#include<qdebug.h>
void ImageProcessor::tempSave(QImage im) {
    if(this->save) {
        this->_img = im;
    }
}

void ImageProcessor::tempSaveNow(QImage im) {
    this->_img = im;
}

void ImageProcessor::saveFile(QString filePath) {
    this->_img.save(filePath);
}
