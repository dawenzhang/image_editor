#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>

#include <QFileDialog>

#include <QGraphicsView>
#include <QGraphicsScene>

#include <QList>

#include "mypixmapitem.h"
#include "imageprocessor.h"
#include "bwdialog.h"
#include "numinputdialog.h"
#include "stretchdialog.h"
#include "spcfltrdialog.h"
#include "oilpaintdialog.h"
#include "reliefdialdialog.h"
#include "matrixdialog.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->actionOpen, SIGNAL(triggered()), this, SLOT(open()));
    connect(ui->actionReset, SIGNAL(triggered()), this, SLOT(reset()));
    connect(ui->btn_Left, SIGNAL(clicked()), this, SLOT(turnLeft()));
    connect(ui->btn_Right, SIGNAL(clicked()), this, SLOT(turnRight()));
    connect(ui->actionRgb2gray, SIGNAL(triggered()), this, SLOT(rgb2gray()));
    connect(ui->actionRgb2bw, SIGNAL(triggered()), this, SLOT(rgb2bw()));
    connect(ui->actionNegative, SIGNAL(triggered()), this, SLOT(negative()));
    connect(ui->actionStretch, SIGNAL(triggered()), this, SLOT(stretch()));
    connect(ui->actionLog, SIGNAL(triggered()), this, SLOT(log()));
    connect(ui->actionHistogramEqualize, SIGNAL(triggered()), this, SLOT(histogramEqualize()));
    connect(ui->actionHistogramExactSpecifiedEqualize, SIGNAL(triggered()), this, SLOT(histogramExactSpecifiedEqualize()));
    connect(ui->actionSpatialFilter, SIGNAL(triggered()), this, SLOT(spatialFilter()));
    connect(ui->actionMedianFilter, SIGNAL(triggered()), this, SLOT(medianFilter()));
    connect(ui->actionFFT, SIGNAL(triggered()), this, SLOT(makeFFT()));
    connect(ui->actionOilPaint, SIGNAL(triggered()), this, SLOT(oilPaint()));
    connect(ui->actionRelief, SIGNAL(triggered()), this, SLOT(relief()));
    connect(ui->actionEdgeExtraction, SIGNAL(triggered()), this, SLOT(edgeExtraction()));
    connect(ui->actionGaussianBlur, SIGNAL(triggered()), this, SLOT(gaussianBlur()));
    connect(ui->actionOpenOperate, SIGNAL(triggered()), this, SLOT(openOp()));
    connect(ui->actionCloseOperate, SIGNAL(triggered()), this, SLOT(closeOp()));
    connect(ui->actionExpansion, SIGNAL(triggered()), this, SLOT(expansionOp()));
    connect(ui->actionCorrosion, SIGNAL(triggered()), this, SLOT(corrosionOp()));
    connect(ui->checkBox, SIGNAL(toggled(bool)), this, SLOT(saveCheck(bool)));
    connect(ui->actionSave, SIGNAL(triggered()), this, SLOT(save()));
    this->ui->graphicsView->setScene(Q_NULLPTR);
    this->pixmapItem = Q_NULLPTR;
    this->directory = new QDir();
    this->imageProcessor = Q_NULLPTR;
    this->ui->actionReset->setEnabled(false);
    this->ui->btn_Left->setEnabled(false);
    this->ui->btn_Right->setEnabled(false);
    this->ui->actionRgb2gray->setEnabled(false);
    this->ui->actionRgb2bw->setEnabled(false);
    this->ui->actionNegative->setEnabled(false);
    this->ui->actionStretch->setEnabled(false);
    this->ui->actionLog->setEnabled(false);
    this->ui->actionHistogramEqualize->setEnabled(false);
    this->ui->actionHistogramExactSpecifiedEqualize->setEnabled(false);
    this->ui->actionSpatialFilter->setEnabled(false);
    this->ui->actionMedianFilter->setEnabled(false);
    this->ui->actionFFT->setEnabled(false);
    this->ui->actionOilPaint->setEnabled(false);
    this->ui->actionRelief->setEnabled(false);
    this->ui->actionEdgeExtraction->setEnabled(false);
    this->ui->actionGaussianBlur->setEnabled(false);
    this->ui->actionSave->setEnabled(false);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::resizeEvent(QResizeEvent *event) {
    this->reset();
}

void MainWindow::showImage(QImage image) {
    if(this->ui->graphicsView->scene() == Q_NULLPTR) {
        this->ui->graphicsView->setScene(new QGraphicsScene(this));
        this->pixmapItem = new MyPixmapItem(QPixmap::fromImage(image));
        this->ui->graphicsView->scene()->addItem(this->pixmapItem);
        this->ui->actionReset->setEnabled(true);
        this->ui->btn_Left->setEnabled(true);
        this->ui->btn_Right->setEnabled(true);
        this->ui->actionRgb2gray->setEnabled(true);
        this->ui->actionRgb2bw->setEnabled(true);
        this->ui->actionNegative->setEnabled(true);
        this->ui->actionStretch->setEnabled(true);
        this->ui->actionLog->setEnabled(true);
        this->ui->actionHistogramEqualize->setEnabled(true);
        this->ui->actionHistogramExactSpecifiedEqualize->setEnabled(true);
        this->ui->actionSpatialFilter->setEnabled(true);
        this->ui->actionMedianFilter->setEnabled(true);
        this->ui->actionFFT->setEnabled(true);
        this->ui->actionOilPaint->setEnabled(true);
        this->ui->actionRelief->setEnabled(true);
        this->ui->actionEdgeExtraction->setEnabled(true);
        this->ui->actionGaussianBlur->setEnabled(true);
        this->ui->actionSave->setEnabled(true);
        this->reset();
        this->imageProcessor = new ImageProcessor(this->pixmapItem->pixmap().toImage(), this);
    }else {
        this->pixmapItem->setPixmap(QPixmap::fromImage(image));
//        this->imageProcessor->setImage(image);
        this->reset();
    }
}

void MainWindow::open() {
    QFileDialog* dlg_Openfile = new QFileDialog(this);
    dlg_Openfile->setFileMode(QFileDialog::ExistingFile);
    dlg_Openfile->setNameFilter("images (*.bmp *.jpg *.png *.tif)");
    if(dlg_Openfile->exec()) {
        QString filePath = dlg_Openfile->selectedFiles().at(0);
        this->showImage(QImage(filePath));
        if(this->ui->graphicsView->scene() != Q_NULLPTR) {
            this->imageProcessor->setImage(QImage(filePath));
        }
        *this->directory = dlg_Openfile->directory();
        this->currentFileName = filePath.right(filePath.count() - this->directory->path().count() - 1);
    }
}

void MainWindow::reset() {
    if(this->pixmapItem != Q_NULLPTR) {
        QRectF rect_PixmapItemRect = this->pixmapItem->boundingRect();
        this->pixmapItem->reset();
        this->ui->graphicsView->scene()->setSceneRect(0, 0, rect_PixmapItemRect.width(), rect_PixmapItemRect.height());
        this->ui->graphicsView->fitInView(rect_PixmapItemRect, Qt::KeepAspectRatio);
        this->ui->graphicsView->centerOn(this->pixmapItem);
    }
}

void MainWindow::turnLeft() {
    QStringList extNames;
    extNames.append("*.jpg");
    extNames.append("*.png");
    extNames.append("*.bmp");
    int num = this->directory->entryList(extNames).indexOf(this->currentFileName);
    num = (num - 1 >= 0) ? (num - 1) : num;
        this->showImage(QImage(this->directory->path() + "/" + this->directory->entryList(extNames).at(num)));
    this->imageProcessor->setImage(QImage(this->directory->path() + "/" + this->directory->entryList(extNames).at(num)));
//    this->pixmapItem->setPixmap(QPixmap(this->directory->path() + "/" + this->directory->entryList(extNames).at(num)));
    this->currentFileName = this->directory->entryList(extNames).at(num);
}

void MainWindow::turnRight() {
    QStringList extNames;
    extNames.append("*.jpg");
    extNames.append("*.png");
    extNames.append("*.bmp");
    int num = this->directory->entryList(extNames).indexOf(this->currentFileName);
    num = (num + 1 <= this->directory->entryList(extNames).count() - 1) ? (num + 1) : num;
    this->showImage(QImage(this->directory->path() + "/" + this->directory->entryList(extNames).at(num)));
//    this->pixmapItem->setPixmap(QPixmap(this->directory->path() + "/" + this->directory->entryList(extNames).at(num)));
    this->imageProcessor->setImage(QImage(this->directory->path() + "/" + this->directory->entryList(extNames).at(num)));
    this->currentFileName = this->directory->entryList(extNames).at(num);
}

void MainWindow::rgb2gray() {
    this->showImage(this->imageProcessor->rgb2gray());
//    this->pixmapItem->setPixmap(QPixmap::fromImage(imageProcessor->rgb2gray()));
}

void MainWindow::rgb2bw() {
    BWDialog* bwDialog = new BWDialog(this);
    connect(bwDialog, SIGNAL(sliderValueChanged(int)), this, SLOT(makeRgb2Bw(int)));
    bwDialog->show();
//    this->showImage(imageProcessor->rgb2bw());
//    this->pixmapItem->setPixmap(QPixmap::fromImage(imageProcessor->rgb2bw()));
}

void MainWindow::makeRgb2Bw(int value) {
    this->showImage(this->imageProcessor->rgb2bw(value));
}

void MainWindow::negative() {
    this->showImage(this->imageProcessor->negative());
}

void MainWindow::stretch() {
    StretchDialog* stretchDialog = new StretchDialog(this);
    connect(stretchDialog, SIGNAL(confirmed(int,double)), this, SLOT(makeStretch(int,double)));
    stretchDialog->show();
}

void MainWindow::makeStretch(int value1, double value2) {
    this->showImage(this->imageProcessor->stretch(value1, value2));
}

void MainWindow::log() {
    NumInputDialog* numDialog = new NumInputDialog(this);
    connect(numDialog, SIGNAL(confirmed(int)), this, SLOT(makeLog(int)));
    numDialog->show();
}

void MainWindow::makeLog(int value) {
    this->showImage(this->imageProcessor->log(value));
}

void MainWindow::histogramEqualize() {
    this->showImage(this->imageProcessor->histogramEqualize());
}

void MainWindow::histogramExactSpecifiedEqualize() {
    this->showImage(this->imageProcessor->histogramExactSpecifiedEqualize());
}

void MainWindow::spatialFilter() {
    SpcFltrDialog* spcFltrDialog = new SpcFltrDialog(this);
    connect(spcFltrDialog, SIGNAL(okClicked(QVector<double>, int)), this, SLOT(makeSpatialFilter(QVector<double>, int)));
    spcFltrDialog->show();
}

void MainWindow::makeSpatialFilter(QVector<double> data, int n) {
//    this->showImage(this->imageProcessor->rgb2gray());
    this->showImage(this->imageProcessor->spatialFilter(data, n));
}

void MainWindow::medianFilter() {
    SpcFltrDialog* spcFltrDialog = new SpcFltrDialog(this);
    connect(spcFltrDialog, SIGNAL(okClicked(QVector<double>, int)), this, SLOT(makeMedianFilter(QVector<double>,int)));
    spcFltrDialog->show();
}

void MainWindow::makeMedianFilter(QVector<double> data, int n) {
    this->showImage(this->imageProcessor->medianFilter(data, n));
}

void MainWindow::makeFFT() {
    this->showImage(this->imageProcessor->fft());
}

void MainWindow::oilPaint() {
    OilPaintDialog* oilPaintDialog = new OilPaintDialog(this);
    connect(oilPaintDialog, SIGNAL(sliderConfirmed(int,int)), this, SLOT(makeOilPaint(int, int)));
    oilPaintDialog->show();
}

void MainWindow::makeOilPaint(int v1, int v2) {
    this->showImage(this->imageProcessor->oilPaint(v1, v2));
}

void MainWindow::relief() {
    ReliefDialDialog* reliefDialDialog = new ReliefDialDialog(this);
    connect(reliefDialDialog, SIGNAL(sliderValueChanged(float)), this, SLOT(makeRelief(float)));
    reliefDialDialog->show();
}

void MainWindow::makeRelief(float ang) {
    this->showImage(this->imageProcessor->relief(ang));
}

void MainWindow::edgeExtraction() {
    this->showImage(this->imageProcessor->edgeExtraction());
}

void MainWindow::gaussianBlur() {
    BWDialog* bwDialog = new BWDialog(this, 10);
    connect(bwDialog, SIGNAL(sliderValueChanged(int)), this, SLOT(makeGaussianBlur(int)));
    bwDialog->show();
}

void MainWindow::makeGaussianBlur(int radius) {
    this->showImage(this->imageProcessor->gaussianBlur(radius));
}

void MainWindow::openOp() {
    MatrixDialog* matrixDialog = new MatrixDialog(this);
    matrixDialog->show();
    connect(matrixDialog, SIGNAL(okClicked(QVector<double>,int)), this, SLOT(makeOpenOp(QVector<double>,int)));
}

void MainWindow::makeOpenOp(QVector<double> data,int n) {
    this->showImage(this->imageProcessor->openOp(data, n));
}

void MainWindow::closeOp() {
    MatrixDialog* matrixDialog = new MatrixDialog(this);
    matrixDialog->show();
    connect(matrixDialog, SIGNAL(okClicked(QVector<double>,int)), this, SLOT(makeCloseOp(QVector<double>,int)));
}

void MainWindow::makeCloseOp(QVector<double> data,int n) {
    this->showImage(this->imageProcessor->closeOp(data, n));
}

void MainWindow::expansionOp() {
    MatrixDialog* matrixDialog = new MatrixDialog(this);
    matrixDialog->show();
    connect(matrixDialog, SIGNAL(okClicked(QVector<double>,int)), this, SLOT(makeExpansionOp(QVector<double>,int)));
}

void MainWindow::makeExpansionOp(QVector<double> data,int n) {
    this->showImage(this->imageProcessor->expansionOp(data, n));
}

void MainWindow::corrosionOp() {
    MatrixDialog* matrixDialog = new MatrixDialog(this);
    matrixDialog->show();
    connect(matrixDialog, SIGNAL(okClicked(QVector<double>,int)), this, SLOT(makeCorrosionOp(QVector<double>,int)));
}

void MainWindow::makeCorrosionOp(QVector<double> data,int n) {
    this->showImage(this->imageProcessor->corrosionOp(data, n));
}

void MainWindow::saveCheck(bool c) {
    this->imageProcessor->save = c;
}

void MainWindow::save() {
    QString filename =QFileDialog::getSaveFileName(this, "save as", "untitled", "Image files (*.bmp *.jpg *.pbm *.pgm *.png *.ppm *.xbm *.xpm);;All files (*.*)");
    if(filename.isEmpty()) {
        return;
    }else {
        this->imageProcessor->saveFile(filename);
    }
}
