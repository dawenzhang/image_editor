#ifndef TOOLS_H
#define TOOLS_H

#include <QVector>
#include <QImage>
#include <QPoint>
#include <QList>
#include <QRgb>
#include <cmath>
#include <complex>
typedef std::complex<double> complex;

struct ImagePixelGray {
    QImage image;
    QPoint point;
};

class Tools {
public:
    static int getGray(QRgb rgb);
    static int getGrayAround(QImage image, QPoint point, int r);
    static bool pixelLessThan(const ImagePixelGray& p1, const ImagePixelGray& p2);
    static void sortAndAdd2List(QImage, QList<QPoint>, QList<QPoint>* );
    static int fft(complex* , int, complex* );
    static QImage* fftImage2Gray(QImage* );
};

#endif // TOOLS_H
