#ifndef STRETCHDIALOG_H
#define STRETCHDIALOG_H

#include <QDialog>

class QLineEdit;

class StretchDialog : public QDialog
{
    Q_OBJECT
public:
    explicit StretchDialog(QWidget* parent = 0);

    bool checkDigit(QString);
    bool checkNumeric(QString);

public slots:
    void onConfirmButtonClicked();

signals:
    void confirmed(int, double);

private:
    QLineEdit* numInput1;
    QLineEdit* numInput2;
};

#endif // STRETCHDIALOG_H
