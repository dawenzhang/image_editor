#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class MyPixmapItem;
class QDir;
class ImageProcessor;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void resizeEvent(QResizeEvent* event);

public slots:
    void open();
    void reset();
    void turnLeft();
    void turnRight();
    void rgb2gray();
    void rgb2bw();
    void makeRgb2Bw(int);
    void negative();
    void stretch();
    void makeStretch(int, double);
    void log();
    void makeLog(int value);
    void histogramEqualize();
    void histogramExactSpecifiedEqualize();
    void spatialFilter();
    void makeSpatialFilter(QVector<double>, int);
    void medianFilter();
    void makeMedianFilter(QVector<double>, int);
    void makeFFT();
    void oilPaint();
    void makeOilPaint(int, int);
    void relief();
    void makeRelief(float);
    void edgeExtraction();
    void gaussianBlur();
    void makeGaussianBlur(int);
    void openOp();
    void makeOpenOp(QVector<double>,int);
    void closeOp();
    void makeCloseOp(QVector<double>,int);
    void expansionOp();
    void makeExpansionOp(QVector<double>,int);
    void corrosionOp();
    void makeCorrosionOp(QVector<double>,int);
    void saveCheck(bool);
    void save();

private:
    void showImage(QImage);

private:
    Ui::MainWindow *ui;
    MyPixmapItem* pixmapItem;
    QDir* directory;
    QString currentFileName;
    ImageProcessor* imageProcessor;

};

#endif // MAINWINDOW_H
