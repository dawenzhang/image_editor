#include "numinputdialog.h"

#include <QLayout>
#include <QPushButton>
#include <QLineEdit>
#include <QDebug>

NumInputDialog::NumInputDialog(QWidget* parent):QDialog(parent)
{
    this->setMinimumSize(200, 200);
    QHBoxLayout* numLayout = new QHBoxLayout();
    this->numInput = new QLineEdit(this);
    this->numInput->setInputMethodHints(Qt::ImhDigitsOnly);
    numLayout->addWidget(this->numInput);
    QPushButton* confirmButton = new QPushButton(this);
    connect(confirmButton, SIGNAL(clicked()), this, SLOT(onConfirmButtonClicked()));
    numLayout->addWidget(confirmButton);
    this->setLayout(numLayout);
    this->setWindowModality(Qt::WindowModal);
}

bool NumInputDialog::checkDigit(QString str) {
    for(int i = 0; i < str.length(); i ++) {
        if(!str.at(i).isDigit()) {
            return false;
        }
    }
    return true;
}

void NumInputDialog::onConfirmButtonClicked() {
    if(!this->numInput->text().isEmpty() && this->checkDigit(this->numInput->text())) {
        emit this->confirmed(this->numInput->text().toInt());
        this->done(0);
    }
}
