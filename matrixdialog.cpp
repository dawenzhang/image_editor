#include "matrixdialog.h"

MatrixDialog::MatrixDialog(QWidget *parent) : QDialog(parent) {
    this->matrixTextEdit = new QTextEdit(this);
    this->button = new QPushButton(this);
    connect(this->button, SIGNAL(clicked()), this, SLOT(on_buttonBox_accepted()));
    QVBoxLayout* layout = new QVBoxLayout();
    layout->addWidget(this->matrixTextEdit);
    layout->addWidget(this->button);
    this->setLayout(layout);
}

MatrixDialog::~MatrixDialog() {

}


void MatrixDialog::on_buttonBox_accepted() {
    QVector<double> data;
    int nCols = - 1;
    bool ok;
    QString strMat=this->matrixTextEdit->toPlainText().trimmed();
    strMat.replace("\n", " ");
    QStringList rows = strMat.split(QRegExp("\\s*;"));
    for(int i = 0; i < rows.length(); i ++) {
        if(rows[i].trimmed().isEmpty()) {
            continue;
        }
        if(nCols < 0) {
            readOneRow(data, rows[i].trimmed(), nCols, ok);
            if(!ok) {
                qDebug() << "parse error!" << endl;
                return;
            }
        }else {
            int tmp;
            readOneRow(data, rows[i].trimmed(), tmp, ok);
            if(!ok) {
                qDebug() << "parse error!" << endl;
                return;
            }
            if(tmp != nCols) {
                qDebug() << "parse error!" << endl;
                return;
            }
        }

    }
    emit okClicked(data, nCols);
}

void MatrixDialog::readOneRow(QVector<double>& data, QString row, int& nCols, bool& ok) {
    nCols = 0;
    QStringList cols = row.split(QRegExp("\\s+|\\s*,"));
    for(int i = 0; i < cols.size(); i ++, nCols ++) {
        if(cols[i].trimmed().isEmpty()) {
            continue;
        }
        double d = cols[i].toDouble(&ok);
        if(ok == false) {
            return;
        }
        if(d != 0 && d != 1) {
            d = 0;
        }
        data.push_back(d);
    }
}

