#ifndef NUMINPUTDIALOG_H
#define NUMINPUTDIALOG_H

#include <QDialog>

class QLineEdit;

class NumInputDialog : public QDialog
{
    Q_OBJECT
public:
    explicit NumInputDialog(QWidget* parent = 0);

    bool checkDigit(QString);

public slots:
    void onConfirmButtonClicked();

signals:
    void confirmed(int);

private:
    QLineEdit* numInput;
};


#endif // NUMINPUTDIALOG_H
