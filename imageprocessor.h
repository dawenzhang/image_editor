#ifndef IMAGEPROCESSOR_H
#define IMAGEPROCESSOR_H

#include <QImage>
#include <QVector>

class ImageProcessor : QObject
{
    Q_OBJECT

public:
    ImageProcessor(QImage, QObject* = 0);

    bool save;

    void setImage(QImage);
    QImage rgb2gray();
    QImage negative();
    QImage stretch(int, double);
    QImage log(int);
    QImage histogramEqualize();
    QImage histogramExactSpecifiedEqualize();
    QImage spatialFilter(QVector<double>, int);
    QImage medianFilter(QVector<double>, int);
    QImage fft();
    QImage oilPaint(int, int);
    QImage relief(float);
    QImage edgeExtraction();
    QImage edgeKernel(QImage);
    QImage gaussianBlur(int);
    QImage expansionKernelSet(QVector<double>, int);
    QImage expansionOp(QVector<double>, int);
    QImage corrosionKernelSet(QVector<double>, int);
    QImage corrosionOp(QVector<double>, int);
    QImage openOp(QVector<double>, int);
    QImage closeOp(QVector<double>, int);
    void tempSave(QImage);
    void tempSaveNow(QImage);
    void saveFile(QString);

private:
    QImage zeroPadding(QImage, int, int);
    QImage replicatePadding(QImage, int, int);
    QImage symmetricPadding(QImage, int, int);
    QImage filter(QImage, QVector<double>, int);
    QImage crop(QImage, int, int);
    QVector<int> getPatch(QImage, int, int, int, int);
    QImage paddingByEdge(int, int);
    QImage cropColor(QImage, int, int);

public slots:
    QImage rgb2bw(int);

private:
    QImage _img;
};

#endif // IMAGEPROCESSER_H
