#ifndef OILPAINTDIALOG_H
#define OILPAINTDIALOG_H

#include <QDialog>

class QSlider;

class OilPaintDialog : public QDialog
{
    Q_OBJECT
public:
    explicit OilPaintDialog(QWidget* parent = 0);

signals:
    void sliderConfirmed(int, int);

public slots:
    void onAccepted();

private:
    QSlider* bwSlider;
    QSlider* intenSlider;
};

#endif // OILPAINTDIALOG_H
