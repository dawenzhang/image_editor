#ifndef BWDIALOG_H
#define BWDIALOG_H

#include <QDialog>

class BWDialog : public QDialog
{
    Q_OBJECT
public:
    explicit BWDialog(QWidget* parent = 0, int = 255);

signals:
    void sliderValueChanged(int);

public slots:
    void onSliderValueChanged(int);
};

#endif // BWDIALOG_H
