#include "mypixmapitem.h"
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsSceneWheelEvent>

MyPixmapItem::MyPixmapItem(QGraphicsItem *parent) :
    QGraphicsPixmapItem(parent)
{
}

MyPixmapItem::MyPixmapItem(const QPixmap &pixmap, QGraphicsItem *parent) :
    QGraphicsPixmapItem(pixmap, parent) {

}

void MyPixmapItem::mousePressEvent(QGraphicsSceneMouseEvent *event) {

}

void MyPixmapItem::mouseMoveEvent(QGraphicsSceneMouseEvent *event) {
    this->setPos(this->pos() + mapToParent(event->pos()) - mapToParent(event->lastPos()));
}

void MyPixmapItem::wheelEvent(QGraphicsSceneWheelEvent *event) {
    this->setTransformOriginPoint(event->pos());
    if(event->delta() > 0) {
        this->setScale(this->scale() * 2);
    }else {
        this->setScale(this->scale() / 2);
    }
}

void MyPixmapItem::reset() {
    this->setPos(0, 0);
    this->setScale(1);
}
