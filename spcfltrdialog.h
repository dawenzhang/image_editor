#ifndef SPCFLTRDIALOG_H
#define SPCFLTRDIALOG_H
#include <QDialog>
class QDialogButtonBox;
class QTextEdit;

class SpcFltrDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SpcFltrDialog(QWidget* parent = 0);

private:
    QTextEdit* filter;
    QDialogButtonBox* buttonBox;

public slots:
    void buttonBoxAccepted();
    void readOneRow(QVector<double>& , QString, int& , bool& );

signals:
    void okClicked(QVector<double>, int);
};


#endif // SPCFLTRDIALOG_H
