#ifndef MATRIXDIALOG_H
#define MATRIXDIALOG_H

#include <QDialog>
#include <QVector>
#include <QDebug>
#include <QRegExp>
#include <QStringList>
#include <QTextEdit>
#include <QPushButton>
#include <QLayout>

class MatrixDialog : public QDialog
{
    Q_OBJECT

public:
    explicit MatrixDialog(QWidget *parent = 0);
    ~MatrixDialog();

private slots:
    void on_buttonBox_accepted();

private:
    void readOneRow(QVector<double>& ,QString, int& ,bool& );
    QTextEdit* matrixTextEdit;
    QPushButton* button;

signals:
    void okClicked(QVector<double> , int);
};

#endif // MATRIXDIALOG_H
