#include "bwdialog.h"
#include <QLayout>
#include <QSlider>
#include <QDebug>

BWDialog::BWDialog(QWidget* parent, int range):QDialog(parent)
{
    this->setMinimumSize(200, 200);
    QHBoxLayout* bwLayout = new QHBoxLayout();
    QSlider* bwSlider = new QSlider(this);
    bwSlider->setRange(0, range);
    bwLayout->addWidget(bwSlider);
    this->setLayout(bwLayout);
    connect(bwSlider, SIGNAL(valueChanged(int)), this, SLOT(onSliderValueChanged(int)));
    bwSlider->setValue(range / 2);
//    this->setWindowModality(Qt::WindowModal);
}

void BWDialog::onSliderValueChanged(int value) {
    emit this->sliderValueChanged(value);
}

