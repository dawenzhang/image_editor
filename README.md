## Image Editor based on Qt Semester 1, 2015

### run

various image processing functions are implemented, from rgb2gray, histogram equalization, to gaussian blur, etc.

an oil paint effect example:

![example 1](./example_1.png)

an angled shadow effect example:

![example 2](./example_2.png)

### __if you find this repo, please do not use any code fragment from it for academic assessment__