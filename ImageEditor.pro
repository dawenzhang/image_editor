#-------------------------------------------------
#
# Project created by QtCreator 2016-02-29T13:41:14
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ImageEditor
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    mypixmapitem.cpp \
    imageprocessor.cpp \
    bwdialog.cpp \
    numinputdialog.cpp \
    stretchdialog.cpp \
    tools.cpp \
    spcfltrdialog.cpp \
    oilpaintdialog.cpp \
    reliefdialdialog.cpp \
    matrixdialog.cpp

HEADERS  += mainwindow.h \
    mypixmapitem.h \
    imageprocessor.h \
    bwdialog.h \
    numinputdialog.h \
    stretchdialog.h \
    spcfltrdialog.h \
    tools.h \
    oilpaintdialog.h \
    reliefdialdialog.h \
    matrixdialog.h

FORMS    += mainwindow.ui
