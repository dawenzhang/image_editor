#include "stretchdialog.h"

#include <QLayout>
#include <QPushButton>
#include <QLineEdit>
#include <QDebug>

StretchDialog::StretchDialog(QWidget* parent):QDialog(parent)
{
    this->setMinimumSize(200, 200);
    QHBoxLayout* numLayout = new QHBoxLayout();
    this->numInput1 = new QLineEdit(this);
    this->numInput1->setInputMethodHints(Qt::ImhDigitsOnly);
    this->numInput2 = new QLineEdit(this);
    this->numInput2->setInputMethodHints(Qt::ImhDigitsOnly);
    numLayout->addWidget(this->numInput1);
    numLayout->addWidget(this->numInput2);
    QPushButton* confirmButton = new QPushButton(this);
    connect(confirmButton, SIGNAL(clicked()), this, SLOT(onConfirmButtonClicked()));
    numLayout->addWidget(confirmButton);
    this->setLayout(numLayout);
    this->setWindowModality(Qt::WindowModal);
}

bool StretchDialog::checkDigit(QString str) {
    for(int i = 0; i < str.length(); i ++) {
        if(!str.at(i).isDigit()) {
            return false;
        }
    }
    return true;
}

bool StretchDialog::checkNumeric(QString str) {
    for(int i = 0; i < str.length(); i ++) {
        if(!str.at(i).isDigit() && str.at(i) != '.') {
            return false;
        }
    }
    return true;
}

void StretchDialog::onConfirmButtonClicked() {
    if(!this->numInput1->text().isEmpty() && !this->numInput2->text().isEmpty() && this->checkDigit(this->numInput1->text()) && this->checkNumeric(this->numInput2->text())) {
        emit this->confirmed(this->numInput1->text().toInt(), this->numInput2->text().toDouble());
        this->done(0);
    }
}
