#include "oilpaintdialog.h"
#include <QLayout>
#include <QSlider>
#include <QPushButton>
#include <QLabel>
#include <QDebug>

OilPaintDialog::OilPaintDialog(QWidget* parent):QDialog(parent)
{
    this->setMinimumSize(200, 200);
    QHBoxLayout* bwLayout = new QHBoxLayout();
    this->bwSlider = new QSlider(this);
    this->bwSlider->setRange(1, 6);
    bwLayout->addWidget(new QLabel("radious"));
    bwLayout->addWidget(this->bwSlider);
    this->intenSlider = new QSlider(this);
    this->intenSlider->setRange(1, 25);
    bwLayout->addWidget(this->intenSlider);
    QPushButton* confirmButton = new QPushButton("confirm", this);
    bwLayout->addWidget(confirmButton);
    this->setLayout(bwLayout);
//    connect(bwSlider, SIGNAL(valueChanged(int)), this, SLOT(onSliderValueChanged(int)));
    connect(confirmButton, SIGNAL(clicked()), this, SLOT(onAccepted()));
    bwSlider->setValue(6);
    intenSlider->setValue(25);
//    this->setWindowModality(Qt::WindowModal);
}

void OilPaintDialog::onAccepted() {
    emit this->sliderConfirmed(this->bwSlider->value(), this->intenSlider->value());
    this->close();
}
