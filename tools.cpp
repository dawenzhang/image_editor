#include "tools.h"

#include <QtAlgorithms>

#include <QDebug>

int Tools::getGray(QRgb rgb) {
    int r = qRed(rgb);
    int g = qGreen(rgb);
    int b = qBlue(rgb);
    int gray = (r + g + b) / 3;
    return gray;
}

int Tools::getGrayAround(QImage image, QPoint point, int r) {
    int gray = 0;
    for(int i = - r; i <= r; i ++) {
        for(int j = - r; j <= r; j ++) {
            if(image.rect().contains(point.x() + i, point.y() + j)) {
                gray += getGray(image.pixel(point.x() + i, point.y() + j));
            }
        }
    }
    return gray;
}

bool Tools::pixelLessThan(const ImagePixelGray& p1, const ImagePixelGray& p2) {
    int r = 1;
    while(getGrayAround(p1.image, p1.point, r) == getGrayAround(p2.image, p2.point, r)) {
            r ++;
    }
    if(getGrayAround(p1.image, p1.point, r) > getGrayAround(p2.image, p1.point, r)) {
        return false;
    }else {
        return true;
    }
}

void Tools::sortAndAdd2List(QImage image, QList<QPoint> originalList, QList<QPoint>* sortedList) {
    QList<ImagePixelGray> appendingList;
    for(int i = 0; i < originalList.length(); i ++) {
        appendingList.append((ImagePixelGray){image, originalList.at(i)});
    }

    qStableSort(appendingList.begin(), appendingList.end(), pixelLessThan);
    for(int i = 0; i < originalList.length(); i ++) {
        sortedList->append(appendingList.at(i).point);
    }
}

int Tools::fft(complex * x, int n, complex * y) {
    int k = n >> 1;
    int i, j; i = j = 0;
    complex w(1.0, 0.0);
    complex wn(cos(2*M_PI/n), sin(-2*M_PI/n));
    complex t(0.0, 0.0);
    complex * x0 = new complex[k];
    complex * x1 = new complex[k];
    complex * y0 = new complex[k];
    complex * y1 = new complex[k];
    if ((NULL == x0) || (NULL == x1) || (NULL == y0) || (NULL == y1)) {
        return 0;
    }
    if (1 == n) {
        y[0] = x[0];
        return 1;
    }
    for (i = 0; i < k ; i++) {
        int j = 2 * i;
        x0[i] = x[j];
        x1[i] = x[j+1];
    }
    if (fft(x0, k, y0) && fft(x1, k, y1)) {
         for (i = 0; i < k; i++) {
            t = y1[i] * w;
            y[i] = y0[i] + t;
            y[i+k] = y0[i] - t; w = w * wn;
         }
     }
    return 1;

}

QImage* Tools::fftImage2Gray(QImage* image) {
    int w = image->width();
    int h = image->height();
    if(w % 2 != 0 || h % 2 != 0) {
        return image;
    }
    complex * td = new complex[w * h];
    complex * fd = new complex[w * h];
    uchar * imageData = image->bits();
    uchar r, g, b;
    int p = 0;
    uchar temp = 0;
    double spectral;
    int lineBytes = image->bytesPerLine();
    for (int i = 0; i < h; i ++) {
        for (int j = 0; j < w; j ++) {
            p = j * 4 + i * lineBytes;
            b = imageData[p];
            g = imageData[p + 1];
            r = imageData[p + 2];

            temp = (uchar)(0.299 * r + 0.587 * g + 0.114 * b);
            td[j + w * i] = complex(temp * pow(- 1, i + j), 0);
        }
    }
    for (int i = 0; i < h; i ++) {
        fft(&td[w * i], 512, &fd[w * i]);
    }
    for (int i = 0; i < h; i ++) {
        for (int j = 0; j < w; j ++) {
            td[i + h * j] = fd[j + w * i];
        }
    }
    for (int i = 0; i < w; i++) {
        fft(&td[i * h], 512, &fd[i * h]);
    }

    for (int i = 0; i < h; i++) {
        for (int j = 0; j < w; j++) {
            spectral = sqrt(fd[j * h + i].real() * fd[j * h + i].real() + fd[j * h + i].imag() * fd[j * h + i].imag()) / 100;
            if (spectral > 255) {
                spectral = 255;
            }

             p = j * 4 + i * lineBytes;
             imageData[p] = (uchar)spectral;
             imageData[p + 1] = (uchar)spectral;
             imageData[p + 2] = (uchar)spectral;
        }
    }
    delete td;
    delete fd;
    return image;
}
